* Ingredients

 - 1lbs ground beef
 - 1lbs ground lamb
 - 1 red bell pepper chopped and then run through a blender
 - 3 gloves garlic finely chopped
 - Smallish yellow onion diced

* Directions

Dice up the vegetables and blend together in a blender. Mix the
result with the meat. Cook.

* Notes

Perhaps run the onion and garlic cloves through the blender with the
bell pepper? Probably.
