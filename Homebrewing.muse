#title  Ale is Neat

Once upon a time I brewed beer. Then I lost access to my roommate's
kettle owing to moving three hundred miles, and there was much sadness
for my poor kit lacked all but this very expensive kettle. A new
kettle has finally fallen into my hands, and so beer may be brewed
again.

* Current Activity

I started brewing again with a few friends (makes it much easier to
actually get around to doing it when you have help). Since multiple
people are involved we are using [[http://wiki.calefaction.org/HomeBrewing][a wiki]] to collaborate. This poor page
will probably remain dusty because I can just put things there
instead.

* More Recent Brews

I decided to at least make a stab at being unlazy and actually linking
to the files that are stored with the website. I keep a reasonable
brewing log (probably dry and boring to almost everyone) in the files
themselves.

I'm only bothering to list recipes that have been brewed and bottled
here--information on anything in the pipeline is at the [[http://wiki.calefaction.org/HomeBrewing][brewing wiki]]
since it is liable to change a lot.

Since old trusty [[http://www.usermode.org/code.html][QBrew]] is no longer actively maintained and [[http://brewtarget.sourceforge.net/][Brewtarget]]
is I've switched over to that. Meaning my newer recipes are in [[http://beerxml.com][BeerXML]]
that theoretically can be imported into other programs, but may very
well only work in Brewtarget.

** English IPA

We had a lot of leftover ingredients after
[[http://pursuing.calefaction.org/2011/11/the-things-i-do-for-love/][Steve's amazing brewing Odyssey]], most of them English grains and
hops. As such I threw together the leftovers into what seemed like it
would be a good approximation of an English IPA. We even threw this
onto the yeast cake of the last batch in the wedding brews (an ESB).

Unfortunately, it was a wee bit underwhemling. Since we have a fancy
temperature controlled fridge now, I decided to brew around 68F (no
risk of overshooting). It came out slightly musty and generally
bland... still, for being made from leftovers it's alright.

 - [[beer-recipes/8-English IPA/english_ipa-8_a.html][Batch A]] 2011-10-16 ([[beer-recipes/8-English IPA/english_ipa-8_a.xml][source]])

** Cernunnos (Ginger-Chamomile Wheat Ale)

Cheap and quick to make, this is ready to drink after a quick
fermentation and as soon as the bottles carbonate. The chamomile
really shines here adding a honey-like note; the ginger adds
additional earthiness and a bit of zing; Sorachi Ace hops finish
everything up with notes of lemon cream.

 - [[beer-recipes/7-Ginger Chamomile Wheat/ginger_chamomile_wheat-7_a.html][Batch A 2011-05-04]] ([[beer-recipes/7-Ginger Chamomile Wheat/ginger_chamomile_wheat-7_a.xml][source]])
 - [[beer-recipes/7-Ginger Chamomile Wheat/ginger_chamomile_wheat-7_b.html][Batch B 2011-07-05]] ([[beer-recipes/7-Ginger Chamomile Wheat/ginger_chamomile_wheat-7_b.xml][source]])
 - [[beer-recipes/7-Ginger Chamomile Wheat/ginger_chamomile_wheat-7_c.html][Batch C 2012-02-18]] ([[beer-recipes/7-Ginger Chamomile Wheat/ginger_chamomile_wheat-7_c.xml][source]])
 - [[beer-recipes/7-Ginger Chamomile Wheat/ginger_chamomile_wheat-7_d.html][Batch D 2012-02-18]] ([[beer-recipes/7-Ginger Chamomile Wheat/ginger_chamomile_wheat-7_d.xml][source]]) - My first ten gallon batch. I lost
   about a gallon, but what can you do.

** Nowruz (India Pale Ale)

Perhaps my finest recipe to date. A suggestion to continuously hop was
heeded and the result was incredible.

 - [[beer-recipes/4-India Pale Ale/india_pale_ale-4_a.html][Batch A]] 2011-03-20 ([[beer-recipes/4-India Pale Ale/india_pale_ale-4_a.xml][source]])
 - [[beer-recipes/4-India Pale Ale/india_pale_ale-4_b.html][Batch B]] 2011-06-11 ([[beer-recipes/4-India Pale Ale/india_pale_ale-4_b.xml][source]]) This adds Centennial and expanded the
   dry hopping a bit -- the Centennial was out of place, and the extra
   dry hopping overpowered the fruity notes present in Batch A. But
   the extra bitterness and attenuation definitely improved things.
 - [[beer-recipes/4-India Pale Ale/india_pale_ale-4_c.html][Batch C]] 2012-01-11 ([[beer-recipes/4-India Pale Ale/india_pale_ale-4_c.xml][source]]) Back to the basics, but adding Citra to
   the boil and using Chico Yeast. First impressions are: even better
   than Batch A!
 - [[beer-recipes/4-India Pale Ale/india_pale_ale-4_d.html][Batch D]] 2012-05-06 ([[beer-recipes/4-India Pale Ale/india_pale_ale-4_d.xml][source]]) Ten gallons, half on US ale yeast, half
   on Wyeast [[http://www.wyeastlab.com/rw_yeaststrain_detail.cfm?ID=136][Belgian Ardennes]] yeast. Initial impressions are that the
   Belgian version is amazing: the fruit flavors and phenols from the
   yeast complement the hopping quite nicely.

** Thoth (Foreign Extra Stout If You Bend a Bit)

An attempt at experimentation--an adjunct (blackstrap molasses), a
spice (star anise), and a reused yeast cake (Ringwood from a
[[http://beeradvocate.com/beer/profile/113/576/][Samuel Smith's Nut Brown Ale]] clone).

 - [[beer-recipes/5-Strong Stout/strong_stout-5_a.html][Thoth]] ([[beer-recipes/5-Strong Stout/strong_stout-5_a.qbrew][source]])

** Pale Ale

My first brew, resurrected.

 - [[beer-recipes/1-Pale Ale/pale_ale-1_c.html][First Time on the New Rig]] ([[beer-recipes/1-Pale Ale/pale_ale-1_c.qbrew][source]])

* Cider / Apfelwein / Cyser

My girlfriend hates carbonation and so, inspired by Edwort's
Applewine, I started experimenting with batches of cider. I'm keeping
some notes, but doing it more by "I guess this is the gravity..." than
with brewing due to the fairly different production
methods. Eventually I hope to get wine making down a bit better...

Occasionally I dump the text notes from my phone to
[[http://wiki.calefaction.org/ClintonEbadi/Cider][a page on Steve's wiki]].

* Long Past Brews

I present to you my poorly annotated [[http://www.usermode.org/code.html][QBrew]] files from my ancient brews
made in the lands of pleasant living.

** Pale Ale

 - [[beer-recipes/1-Pale Ale/pale_ale-1_a.html][First Brew Ever]] ([[beer-recipes/1-Pale Ale/pale_ale-1_a.qbrew][source]])
 - [[beer-recipes/1-Pale Ale/pale_ale-1_b.html][Revision B]] ([[beer-recipes/1-Pale Ale/pale_ale-1_b.qbrew][source]])

** Porter

 - The record of the first brew has been lost in time
 - [[beer-recipes/2-Porter/porter-2_b.html][Second Attempt]] ([[beer-recipes/2-Porter/porter-2_b.qbrew][source]])

** Wheat Ale

 - [[beer-recipes/3-Wheat Ale/wheat_ale-3_a.html][Bannanas]] ([[beer-recipes/3-Wheat Ale/wheat_ale-3_a.qbrew][source]]) -- A very hot fermentation made this taste like
   liquid bannanas it was ... interesting.
