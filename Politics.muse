#title Do Not Accept the Weak State of Mind in Our Time

I have views that could perhaps be seen as odd. Do note that I am **not**
a liberal; nor am I a conservative. I do not buy into the traditional
socieconomic dipole scale, and I also reject the *political compass* two
dimensional scale; my political belief system could best be described
as *curmudgeonly bastard* if you must have a label for it. This is only
because being a curmudgeonly bastard isn't an ideology, but rather a
broad set of ideas centered around the rejection of traditional
political and social structures (*i.e* hating everything). I reject the
*ressentiment* (lookit I'm Nietzsche) of traditional anarchism and
believe not that every man should have no master (for then *all* would
be weak), but rather that he should be his own master (does that even
*mean* anything? Eh, it sounds nice so who cares).

These short essays are mere stubs I wrote a long while ago, and each
will perhaps be extended in the future.

* [[Wisdom][The Basis of My Philosophy]]

I read some things and thought they were cool. Now I can make people
think I'm smarter than I really am.

* The Current Economic and Political Structure Is Broken

** The Government of the Unites States

I feel that the government in the United States is very close to being
broken beyond repair (perhaps this is a bit conservative, but one must
hope). As it stands the government above the local level (and even
there!) ignores the individual citizen and instead is only forced to
do anything by large scale action. As far as the individual is
concerned we no longer live in a Republic, but rather in an Plutocracy
which is quickly descending into something far worse.

*** Healthcare *Reform*

Upon airing my objections to the current Healthcare *Reform* bill, I was
asked: did you read the bill? To which I replied with action and read
[[http://www.govtrack.us/congress/bill.xpd?bill=h111-3590&tab=summary][the official summary of the bill]]. And now I ask those who asked me if
I had read it: have *you* read it. I received as a reply an unequivocal:
*why should I have to*.

Fun fact: it isn't as bad as some people make it seem, but guess what?
*It does nothing beneficial to the individual*. All it does it require
that anyone over 30 purchase insurance, severely restricts the usage
of Health Savings Accounts (which, may be not so bad--I have no
opinion on their usefulness... but *Republicans* created them so they
*must* be *evil*), and is generally a piece of hey-look-I-did-something
(but nothing goes into effect until I am out of office)
legislation. This has never happened before, obviously. We are on the
surface of Mars now too didn't you know.

Meanwhile there is what amounts to no price controls, an actual *ban* on
the formation of State run healthcare (until 2017, and then only at
the discretion of the HHS secretary), and token (unfunded) support for
the formation of healthcare cooperatives. There are some taxes on
large drug makers, but the research required by the FDA for drug
approval is *tax deductible* (and so the larger drug makers can avoid
most of the new taxes, har). And... an excise tax on overly fancy
healthcare plans... more or less, a nice bill that, if it manages to
not be overturned by 2018, will do absolutely nothing one way or the
other.

It is obvious that I am indeed a dirty Nazi redneck terrorist
teabagger Republican piece of shit who hates the poor and black
people. I guess it's time for my white ass to move to Iran and see how
I like it there!

** Capitalism is Intrinsically Evil

Cooperation is better than exploitation. How can one justify an
economic system based upon paying others as little as possible in an
attempt to make the most profit from their labor so as to make some
profit?

But then again, what does *evil* mean?

* Misc

** Long Term Copyright Causes Harm to Society (=Draft Revision 2=)

; maybe reinsert intro [[clintons-plans#Writing]]

; - Craft work compensated directly

It is straightforward to calculate a fair cost for material goods. The
material cost follows from the materials, and the labor cost generally
derives from the complexity of construction. The fixed price for each
item consists of both of these factors. Thus it is trivial to ensure
that a craftsman is fairly compensated for his effort.

;  - Creative work indirectly
;    - Harder to regain effort spent on creating

Creative works must have their value calculated via a more circuitous
route. The physical form of a creative work is of little importance;
the ideas it represents are. The material and direct labor costs
(printing, binding, etc.) are thus so small as to be of negligible
importance when calculating value. There is effectively no objective
way to place value on abstract work; all the value judgements we can
make are subjective. We must then rely on irrational human valuations
to determine the value on their own.

;  - Works contain ideas
;    - Focus on written works
;  - Inherent nature of ideas
;    - Absorbed into the mind of the reader
;      - Freely copied orally, libraries, ...
;    - Absorbed into the culture


Creative works are fundamentally different from concrete works. A
painting may inspire others start a new stylistic movement, the
structure of a story may cause the formation of a new literary form,
an essay may incite a new political movement, etc. Creative works
weave themselves into the mental fabric of each individual exposed to
them in a way that material goods cannot. A book may change your life;
a table will never do that. This suggests that the abstract concepts
which compose a work have a strange nature and great value. Those who
control the distribution of creative works wield great power as a
result of the ability of ideas to change the individual.

After a certain period of time the physical manifestation of a
creative work loses commercial value. New art is being created
continually, and no one can be expected to read every important book
written, see every film, and so on for other areas. When a work ceases
to be profitable to publish distribution ceases. Allowing abstract
works to simply drop from the market creates a serious problem. New
ideas are built upon old ones, and after ideas have assimilated into
the collective concious it is important to be able to go back to the
old ideas and analyze them to understand the present culture. If a
work is no longer available it is impossible to do this. Thus works
that are no longer being commercially exploited should become the
property of the public so that any worth preserving will be preserved
by *someone* and avoid death.
    
;  - Copyright helps authors
;    - Gives reasonable period for ideas to be commercially exploited

Copyright manages to work fairly well for ensuring creators are
compensated for their effort, preventing abuse of creator rights to
the detriment of society, and ensuring that works will become public
property after they are commercially unprofitable. Irrational human
judgements over time often work well, and so giving exclusive right to
copy a work makes sense for a period of time to allow society to
determine its monetary value. The fair use provisions of copyright
give society reasonable leeway in the use of the ideas contained
within a work while the work is protected, and this allows society to
continue enriching its creative culture. The limited term of copyright
and ensuing reversion to the public domain prevents the cultural
stagnation and the loss of history that would result from works
becoming unavailable.

;  - Copyright should be short
;    - Purpose is to give the creator time to compensate himself for the
;      effort spent writing
;    - Works often have short commercial life (cite)

The term of copyright must be finely balanced between the need to
ensure creators have enough time to receive fair compensation for
their effort, and the desire to avoid cultural stagnation from
unavailable works. The term must be short enough that a work will not
be unavailable for too long after commercial interest dies. Every year
that passes where the work isn't being published tends to reduce the
number of copies in existence. It must also be long enough that a
creator can profit according to the value that society puts upon his
work.

A term should be just long enough that a work will fall out of
copyright when physical copies are still likely to exist. A man may
keep his book collection unto his death, but his children may simply
sell them off or discard them after he departs the mortal
coil. Intuitive judgement says that things that are worth entering the
public domain will be preserved by someone for at least his life. A
person who has creative works in his posession is often attached to
them and will keep the ones he likes the most for as long as possible
(e.g. my music collection is backed up in flac so that I will be able
to listen to my music forever). After he dies there is a large
increase in the chance that the works will perish unless he by chance
made special arrangements to have them preserved.

A generation then seems to be a reasonable term; how many things are
really commercially viable after thirty years? Some works may be
relevant to the children of the generation who created them; it seems
reasonable then that if a work is still commercially viable after a
generation then the creator deserves to retain copyright for a second
generation. It is questionable whether more terms would be good
(issues of supression of information, right to profit, etc. come into
play), but they can't quite be ruled out. A renewal system with a span
of roughly thirty years ensures that a work will be out of publication
for at most a generation's time. This appears to be a good balance
between the right of the creator and the desire to keep knowledge from
dying (from my eyes).

The works of the current generation, their parents, their
grandparents, and their great-grandparents are still copyrighted in
the US. Works created in the present will be copyrighted for the
lifetime of the author and seventy years after; a span of roughly six
generations.

** Fewer Laws Are Better

*** Individuals should not have their actions regulated

*** Corporations must have their actions heavily regulated

Corporate power disrupts the functioning of a free society. If the
power wielded by a corporation were merely the sum of the individuals
that composed it there would be little issue; the fundamental problem
is that the benefits of gaining access to mass production facilities
and a huge workforce that can be forced to cooperate on certain goals
gives a large corporation much more than this.

**** Corporate Personhood should be revoked

**** Corporations should not be allowed to influence politics

* Social Ills

** Mass Culture

American culture in the early 1900s began to homogenize, and now there
is a single massive culture that almost all three hundred million
people in the country share. This presents problems to those who do
not fit in; in the days of the self sufficient village one could move
to another location to find people similar to him, but now there is
nowhere to go. Everywhere a *social deviant* goes he will feel alienated
and have his social options severely limited.

A monoculture reduces the rate of idea formation, and ours is actively
hostile toward anything not falling in line with the
mainstream. People are trained to act as a mass instead of as
individuals; this results in far less creative people. Critical
thinking is not encouraged; no, it is far worse! Critical thinking is
discouraged, and those of us who wish to argue our points with logic
are met with the undefeatable enemy of a closed mind that has been
exposed to propaganda from birth.

*** The Talking Heads

Poison the well. Burn a strawman.

This is real argument. Real thought.

** The Automobile

; How far is your average trip in a car? If you don't often go further
; than twenty miles have you thought about getting rid of your car?
; Twenty miles! Quite the distance, isn't it? In reality it is a short
;  [[Bicycle][bicycle]] ride that is often under or only slightly longer than an hour
; long! If this pathetic nerd can do it so can you!

; We have finite natural resources, and oil is a resource that we have
; foolishly exploited to the point of exhaustion. Ethanol and other
; biofuels are pipe dreams, and you **shall** have no choice but to learn to
; live without a car as oil is going to increase in cost substantially
; over the next twenty years. Why wait until you are forced to give up
; your car to do so? It makes more economic sense to give it up now
; rather than spend more and more of your income every year just to
; travel. Even ignoring that aspect the confidence it fills you with is
; quite wonderful; there was a time when I walked staring at the ground
; fearful of the world, and now I stand tall and can stare a driver in
; the eyes and tell him to go ahead and try to run into me because I'm
; not giving up my ground.

; Try self transport; it is good for your body and mind. The relative
; low cost of automobiles has forced us into a false sense of needing to
; be transported by machine. We are humans; the lone bipedal upon this
; planet. We were born to transport ourselves!

** Learned Ignorance and Weakness

[[Old Viewpoints][obsolete]]

[[TRUTH]]
