#title Scheme Your Windows

* Installing SCWM

** Dependencies

*** Required

 - [[http://www.gnu.org/software/guile][Guile]] 1.8.0+
 - IMLib 1.x (in Debian imlib11-dev)

*** Optional

 - [[ftp://ftp.gnu.org/gnu/guile-gtk/guile-gtk-0.60.tar.gz][Guile-GTK]] 0.60 for dialog support. Guile-GTK 2.x and scwm are not
   compatible yet

** Downloading and Building

A working version of SCWM can be obtained from
[[http://sourceforge.net/cvs/?group_id=225][sourceforge CVS]]. Compilation is much more straightforward than one
would suspect from a project so old (thanks to dsmith for doing the
painful parts of unbitrotting). The standard autoconf sequence of
commands should work.

<example>
./autogen.sh
./configure
make
sudo make install
</example>

** Running

The directory =sample.scwmrc= has a few sample SCWM
configurations. =system.scwmrc= and =sample.scwmrc= both work fairly well;
I have yet to test the others. If not using =system.scwmrc= it is useful
to at least include the bits that enable debugging.

<src lang="scheme">
(define debug #t)

(if debug
    (begin
      (add-hook! module-loaded-hook display-module-loaded)
      (set! %load-verbosely #t)
      (debug-enable 'debug 'backtrace)
      (read-enable 'positions)))
</src>

SCWM will attempt to load =system.scwmrc= by default, but this can be
overridden with the =-f= option. The default =system.scwmrc= loads user
config from a series of files in =~/.scwm/=.

** Hacking

*** XNested Setup

The easiest way to hack on SCWM is to run it inside of Xnest.

<example>
Xnest :1 &
scwm --display :1 --debug # --debug makes X requests synchronous
</example>

*** GDS for Interaction

SCWM has an emacs interaction mode that communicates with the window
manager using X window properties, but the emacs side of the code
doesn't quite work, and GDS is featureful enough now to be used to
interact with scwm. Using GDS makes hacking scwm quite a bit
easier. **GDS requires a Guile built with threads to work properly
(the version in Debian is built *without* threading).**

**** scwmrc code

The following code should be in your scwmrc somewhere.

<src lang="scheme">
(use-modules (ice-9 gds-client)
	     (ice-9 threads))

;;; GDS thread (in case it must be killed during debugging, ...)
(define cke-gds-thread #f)

(define (connect-to-debugging-server)
  (set! cke-gds-thread
	(call-with-new-thread (lambda () (run-utility))))
  cke-gds-thread)
</src>

After defining you can either use =scwmrepl= to run
=(connect-to-debugging-server)= after starting a GDS server, or simply
add =(connect-to-debugging-server)= to the config if one will always be
running. 

**** Emacs config

I wrote [[http://darcs.unknownlamer.org/site-emacs/init.d/guile-debug.el][a bit of elisp]] to simplify setting up a scratch buffer for
interaction with guile.

<src lang="emacs-lisp">
(require 'gds)
(require 'gds-server)
(require 'gds-scheme)

(defvar guile-scratch-buffer-name "*guile-scratch*"
  "Name of the Guile evaluation scratch buffer")

(defun make-guile-scratch-buffer ()
  (interactive)
  (let ((scratch-buf (generate-new-buffer guile-scratch-buffer-name)))
    (switch-to-buffer scratch-buf)
    (scheme-mode)
    (gds-auto-associate-buffer)))
</src>

If you were already running a debugging client process you will need
to choose the process to associate the buffer with; =ps= will tell you
the PID of scwm.

**** Known Issues

Occasionally evaluating an expression or viewing a backtrace will make
scwm lockup. My understanding is that scwm is not threadsafe, and
there are some evil issues that will take a while to solve. GDS works
well enough for now; if I can't make GDS completely reliable I shall
unbitrot the scwm emacs interaction mode (unless someone else beats me
to it, *hint hint*).
