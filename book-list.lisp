(((|Alan| |Moore|)
  nil
  ("Watchmen" :fiction 8)
  ("V for Vendetta" :fiction 10))
 ((|Neil| |Gaiman|)
  nil
  ("The Sandman (series)"
   :fiction 10
   "Perhaps the best comic book series of all time; I would say *The
Sandman* as a whole ranks higher than anything even Alan Moore has
written.")
  ("Good Omens"
   :fiction 8
   "A friend of a friend decided one evening that I needed to read
so-called *normal people books*, and so she lent me *Good Omens*. It
was an enjoyable read and unearthed vague memories of comic book
magazines I read when I was small and the name *Sandman*; thus through
one book I found something far greater.")
  ("American Gods"
   :fiction 6
   "Entertaining, but the end was a bit much rushed."))
 ((|William| |Blake|)
  "Blake is my [[William Blake][favorite]] of the English poets. His
unique use of relief etching and watercoloring makes for very
interesting Illuminated works. There is a very high quality
[[http://blakearchive.org][complete archive of Blake's works]] online
with high resolution plate scans and full transcriptions among other
things."
  ("The Four Zoas"
   :fiction 10
   "The unfinished manuscript of Blake's longest apocalypse. The
Four Zoas divide from Albion and rage through the ages of dismal woe
to bring about the end of the cycle of Ulro and restore the cycle of
Beulah.")
  ("Jerusalem" :fiction 10 "The finest of Blake's Illuminated works."))
 ((|Kahlil| |Gibran|)
  "Kahlil Gibran is fairly interesting; his earlier works do not
agree with my æsthetic sense (blah blah), but *The Madman* onward are
all rather nice. A few of his works are
[[http://leb.net/~mira/][online]], but I recommend scouting used book
stores for old hardcover editions. The (late 90s onward at least)
*hardcover* versions from *Alfred A. Knopf* are in fact permabound
paperbacks with a hardcasing, and are of seriously inferior quality to
the editions from the 50s and 60s (and cost quite a bit more,
naturally)."
  ("A Tear and a Smile"
   :fiction 3
   "One of Kahlil Gibran's earlier works, I did not much like *A
Tear and a Smile* excepting the last poem (\"A Poet's Voice\").")
  ("The Prophet" :fiction 9)
  ("Sand and Foam" :fiction 7 "An interesting little book of aphorisms.")
  ("The Madman" :fiction 8))
 ((|John| |Taylor| |Gatto|)
  "Former teacher and now author-activist."
  ("Underground History of American Education"
   :nonfiction 9
   "An interesting *underground* history of the American education
system. Available
[[http://www.johntaylorgatto.com/underground/][online for free]]."))
 ((|Luke| |Rhinehardt|)
  nil
  ("The Dice Man"
   :fiction 7
   "<quote>
And it's his illusions about what
constitutes the real world which are
inhibiting him...
His reality, his reason, his society
...these are what must be destroyed
</quote>

A quotation from one of my [[http://en.wikipedia.org/wiki/Slaughter_of_the_Soul][favorite metal songs]] inspired me to grab
this book; at worst it would be a waste of time. Much reward was found
in this random stab in the dark. The book is framed as an
autobiography of the author as a psychoanalyst, and his progression
through life as a Dice Man after deciding to live his life through
random chance.

The style, plot, and content are equally neurotic; part comedy, part
attack on psychoanalysis, and part deep philosophy. It was often
difficult to put down, and was read in under a week of spare time."))
 ((|Neal| |Stephenson|)
  nil
  ("Snow Crash"
   :fiction 9
   "As one must read the *Bible* to understand English literature, so one
must read *Snow Crash* today to be a nerd. In the realm of modern pop
fiction this is one of the better books I've read; it was devoured in
a mere four nights. Neal Stepheson may not be Milton, but he does come
up with enganging tales. *Snow Crash* has a nice undertone of (quite
accurate) political and social commentary that makes it worth reading
as more than mere cyberpunk fiction.")
  ("Cryptonomicon"
   :fiction 8
   "I read *Cryptonomicon* when it was new, and at the time I thought it was
good. It could have lost a hundred or so pages without detracting from
the plot, but it was easy reading and didn't take very long to
finish. The story was enganging, and the continual switching between
the 1940s and present day slowly unravelled the tale in a nice way.

I'd still have to recommend *Snow Crash* if one wished to read only one
Stephenson novel."))
 ((|Marcus| |Aurelius|)
  nil
  ("Meditations"
   :nonfiction 4
   "At the time, I enjoyed reading this collection of meditations on
Stoic philosophy, and it was a fairly quick read (fifteen minutes a
day over the course of two weeks for me). Nowadays I've read
Epictetus, and I suggest reading his *Discourses* instead."))
 ((|Søren| |Kierkegaard|)
  "Kierkegaard was a master of style and philosophy; his writing is
interesting even if one finds the theistic extentialism espoused
disagreeable."
  ("Sickness Unto Death"
   :nonfiction 10
   "I purchased this when I was looking through books at a store after
being unable to find the book I really wanted, and I must say that it
was better for me to have found this one. 

Contained within is a beautiful analysis of despair in the context of
Christianity (really theism in general). Even if the argument offends,
the presentation cannot. The dialectical nature of despair is
reflected in every aspect of the work, and the method of presentation
forces reflection.")
  ("Either/Or"
   :nonfiction 10
   "Composed of two portions, *Either/Or* is a rather lengthy but
rewarding read. The first book is a series of essays and a diary of a
young esthetician; the second is a pair of long letters from an older
ethicist friend to this esthetician. You are then left to resolve the
conflict between the views.")
  ("Fear and Trembling"
   :nonfiction nil
   "An interesting dialectical lyric contrasting Despair and Faith.")
  ("Repetition"
   :nonfiction 10
   "He who despairs of esthetic repetition gets none; he who despairs
of ethical repetition receieves the esthetic. Is it true then that no
repetition exists? Is transition all one can hope for?")
  ("The Concept of Anxiety"
   :nonfiction 7
   "...Very clearly an early work of Kierkegaard. It is rather formal
and difficult to get through. I'd recommend reading a lot of other
Kierkegaard before this. "))
 ((|Thomas| |More|)
  nil
  ("Utopia"
   :fiction 7
   "I read most of Utopia in high school with the TI-89 ebook reader, but
the way the book was split up made it a bit difficult to grasp the
overall structure. I found a copy at a used book store one day, and so
I read it again, and found it much more comprehensible. It is a quick
read, and decent piece of literature. The interesting social system
espoused resembles resembles state communism (even if perhaps as a
negative ideal), but with an strange blend of 14th century European
social customs."))
 ((|William| |James|)
  nil
  ("The Varieties of Religious Experience"
   :nonfiction 7
   "[[William James - The Varieties of Religious Experience][A partially finished extended summary]]")
  ("The PhD Octopus"
   :nonfiction nil
   "<quote>
America is thus as a nation rapidly drifting towards a state of things
in which no man of science or letters will be accounted respectable
unless some kind of badge or diploma is stamped upon him, and in which
bare personality will be a mark of outcast estate.  It seems to me high
time to rouse ourselves to consciousness, and to cast a critical eye
upon this decidedly grotesque tendency.  Other nations suffer terribly
from the Mandarin disease.  Are we doomed to suffer like the rest?
</quote>

[[William James - The PhD Octopus][Full Text]]"))
 ((|Henry| |James|)
  "The novelist brother of William James; I've not read many (read:
one) of his books, but what I did was decent."
  ("The Altar of the Dead"
   :fiction 7
   "A short novella about a man who maintained an altar in a church
for all of his lost loved ones on the surface, but something a bit
more beneath."))
 ((|Gregor| |Kiczales|)
  nil
  ("The Art of the Metaobject Protocol"
   :nonfiction 10
   "AMOP is useful as a reference to the CLOS MOP (although less so with
the online MOP spec), but the true value of the book lies in the first
half of the book. It presents the design of the CLOS MOP through a
series of revisions that fix limitations of earlier implementations
and gradually work toward a generic and well designed MOP for
CLOS. Through that process one is made more aware of a few general
object protocol design skills, and gains insight into how to cleanly
make mapping decisions customizable."))
 ((|Friedrich| |Nietzsche|)
  "A bit acerbic and esoteric, Nietzsche is for me a good *secular*
counterpart to Kierkegaard's theistic philosophy. Nietzsche's
polemical works raise important questions for anyone who reads works
on ethics. As such it is a shame that he has gotten a bad reputation
by being read by far too many angsty teenagers who see (and relay)
only Nietzsche the asshole rather than Nietzsche the master of the
polemic."
  ("Thus Spoke Zarathustra"
   :fiction 8
   "A masterpiece of indirect communication depsite the occasional
flaw and overly dramatic passage. Certainly a book worth reading many
times over the course of one's life.")
  ("Beyond Good and Evil"
   :nonfiction 8
   "A somewhat more comprehensible, if a bit less aesthetically
pleasing, presentation of much of the philosophy found in *Thus Spoke
Zarathustra* in the negative form. The final chapters are very
important (not to detract from the value of the rest of the work) if
one wishes to understand *On the Genealogy of Morals*.")
  ("On the Geneaology of Morals"
   :nonfiction 9
   "*On the Geneaology of Morals* is a wonderful book of three
polemical essays on the origin of moral/ethical valuations, and the
blindness of modern philosphers whose very thinking is tainted by
these valuations unknowingly.")
  ("Ecce Homo"
   :nonfiction 7
   "*Ecce Homo* is Nietzsche's very strange autobiography and
explanation of his own works. At points it is clear that it could have
used a bit more editing (prevented by Nietzsche ... falling into a
catatonic state and all), but is still a very useful book to read as
Nietzsche explains the overall structure of his works."))
 ((|Aristotle|)
  nil
  ("Ethics"
   :nonfiction nil)
  ("Categories"
   :nonfiction nil)
  ("Poetics"
   :nonfiction nil)
;;;   ("Prior Analytics"
;;;    :nonfiction nil
;;;    "*Prior Analytics* is essential reading if one wishes to understand
;;; [[Term Logic][traditional logic]]. Given that traditional logic is
;;; used by most philosophers prior to the mid-1800s it is a *bit*
;;; important. Luckily *Prior Analytics* is
;;; [[http://etext.library.adelaide.edu.au/a/aristotle/a8pra/index.html][available online for free]] and is fairly short.")
  ("Rhetoric"
   :nonfiction nil))
 ((|Aristophanes|)
  nil
  ("The Frogs" :fiction nil)
  ("The Clouds" :fiction nil)
  ("Ecclesiazusae" :fiction nil))
 ((|Plato|)
  nil
  ("Symposium" :fiction nil)
  ("Euthyphro" :fiction nil)
  ("Apology" :nonfiction nil)
  ("Crito" :fiction nil)
  ("Phaedo" :nonfiction 10)
  ("Protagoras" :fiction nil))
 ((|Aeschylus|)
  nil
  ("Oresteia":fiction 10)
  ("Prometheus Bound" :fiction 9)
  ("The Persians" :fiction 8))
 ((|Homer|)
  nil
  ("The Odyssey" :fiction 10))
 ((|George| |Orwell|)
  nil
  ("1984" :fiction 10)
  ("Animal Farm" :fiction nil))
 ((|Aldous| |Huxley|)
  "Perhaps the most overrated modern writer. Other people have written
everything he has to write better and many years before he got around
to it."
  ("The Doors of Perception"
   :nonfiction 0
   "Huxley stains the name of Blake by naming this horrible
pseudo-scientific and pseudo-poetic essay after a line from *The
Marriage of Heaven and Hell*. Subjectivity and objectivity are
incommensurable; his attempt and being subjectively objective is
utterly worthless.")
  ("Heaven and Hell"
   :nonfiction 0
   "Blah blah LSD blah blah Mushrooms blah blah Peyote blah blah I'm
Aldous Huxley I'm a pretentious jerk. Don't bother.")
  ("Brave New World"
   :fiction 7
   "A nice light read; the story is obvious and by the hundreth page
the ending is clear, but it provided a bit of a break from heavier
reading for me. I must say that anyone who has read *Brave New World*
and does not despise modern society has the intellectual capacity of
an *Epsilon*. *1984* is perhaps easily misread, but *Brave New World*
is very clear with its message and is a bit like being smacked upside
the head with a hammer."))
 ((|Douglas| |Adams|)
  nil
  ("Hitchiker's Guide to the Galaxy (collected)" :fiction 8)
  ("The Long Dark Tea-Time of the Soul" :fiction 6))
 ((|H.G.| |Wells|)
  nil
  ("The Island of Dr Moreau" :fiction 7))
 ((|JRR| |Tolkien|)
  nil
  ("The Lord of the Rings" :fiction 9)
  ("The Silmarillion" :fiction 10)
  ("The Lost Tales" :fiction 7))
 ((|Bjarne| |Stroustrup|)
  nil
  ("The C++ Programming Language (3rd edition)"
   :nonfiction nil
   "Once upon a time I was fifteen and I read this book. It was more
or less what taught me how to write programs just large enough to do
useful things, and so shall forever be remembered by me. A year and a
half later I stumbled upon a little language called Scheme and fell
down the rabbit hole."))
 ((|Confucius|)
  nil
  ("Analects" :nonfiction nil))
 ((|Mencius|)
  nil
  ("Mencius" :nonfiction nil))
 ((|Walter| |Miller|)
  nil
  ("A Canticle for Leibowitz" :fiction 10))
 ((|David| |Lamkins|)
  nil
  ("Successful Lisp"
    :nonfiction 8
    "After learning Scheme, I read *Successful Lisp* and was able to
pick up Common Lisp fairly easily."))
 ((|John| |Allison|)
  "The author of the rather amazing [[http://scarygoround.com][Scary Go Round]]. 
I highly recommend procuring the printed collections; the printing
quality is superb (full color on glossy paper), and the long story
arcs are much easier to read."
  ("Looks, Brains and Everything" :fiction nil)
  ("Blame the Sky" :fiction nil)
  ("Skellington" :fiction nil)
  ("The Retribution Index" :fiction nil)
  ("Great Aches" :fiction nil)
  ("Ahoy Hoy!" :fiction nil)
  ("Heavy Metal Hearts and Flowers" :fiction nil)
  ("Ghosts" :fiction nil))
 ((|Mike| |Carey|)
  nil
  ("Lucifer (series)"
   :fiction 6
   "Of the *Sandman* spinoffs, *Lucifer* stands out as the best for
the first half, but then the writer appears to take on far too great a
task, and, with the introduction of some disagreeable character
relations, fails to execute the story as well as it could have
been. Still, it was worth reading to the end even though most of the
stories after issue 35 or so were merely ok. If you like Kierkegaard I
suggest issues 2, 3, and 62--they show the form of the incommensurable
relation of the single individual to the absolute perfectly."))
 ((|Anonymous|)
  nil
  ("Chymical Wedding of Christian Rosenkreutz" :fiction nil))
 ((|Alisa| |Kwitney|)
  nil
  ("Destiny: A Chronicle of Deaths Foretold" :fiction 8))
 ((|John| |Milton|)
  nil
  ("Paradise Lost" :fiction 10))
 ((|Yevgeny| |Zamyatin|)
  nil
  ("We" :fiction))
 ((|Kurt| |Vonnegut|)
  nil
  ("Cat's Cradle"
   :fiction 9
   "There are few books that I have started to read before sleeping
and found myself watching the sun rise after finishing. *Cat's Cradle*
is definitely required nerd reading."))
 ((|Robert| |Anton| |Wilson|)
  "Or rather, Robert Anton Wilson and Robert Shea (but my book script
updating thing doesn't do multiple authors"
  ("The Illuminatus! Trilogy"
   :nonfiction 10
   "e-cash MP5K-SD Adriatic Bellcore Lon Horiuchi 9705 Samford Road
jihad New World Order AVN FTS2000 ANZUS subversive SAPO PET Armani"))
 ((|Edgar| |Allan| |Poe|)
  "ULTRAGOTHIK"
  ("Tales of Mystery and Suspense"
    :fiction 6
    "This is when I learned that I still don't really like late 1800s
American literature all that much. Some of the tales were worth
reading, but most of it was not in a style I like all that much."))
 ((|Albert| |Camus|)
  nil
  ("The Plague" :fiction)))


